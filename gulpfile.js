"use strict";

const gulp = require("gulp");
const browserSync = require('browser-sync').create(); // сервер для работы и автоматического обновления страниц
const plumber = require('gulp-plumber'); // модуль для отслеживания ошибок
const rigger = require('gulp-rigger'); // модуль для импорта содержимого одного файла в другой
const sourcemaps = require('gulp-sourcemaps'); // модуль для генерации карты исходных файлов
const sass = require('gulp-sass'); // модуль для компиляции SASS (sass) в CSS
const autoprefixer = require('autoprefixer'); // модуль для автоматической установки автопрефиксов
const uglify = require('gulp-uglify'); // модуль для минимизации JavaScript
const cache = require('gulp-cache'); // модуль для кэширования
const imagemin = require('gulp-imagemin'); // плагин для сжатия PNG, JPEG, GIF и SVG изображений
const jpegrecompress = require('imagemin-jpeg-recompress'); // плагин для сжатия jpeg
const pngquant = require('imagemin-pngquant'); // плагин для сжатия png
const del = require('del'); // плагин для удаления файлов и каталогов
const postcss = require('gulp-postcss');

const path = {
    dist: {
        html: 'dist/',
        js: 'dist/js/',
        jsmain: 'dist/js/',
        css: 'dist/css/',
        img: 'dist/img/',
        fonts: 'dist/fonts/',
        otherprojects: 'dist/projects/'
    },
    app: {
        html: 'app/*.*',
        js: 'app/js/*.js',
        jsmain: 'app/js/*.js',
        sass: 'app/sass/*.sass',
        css: 'app/css/',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*',
        otherprojects: 'app/otherprojects/**'
    },
    watch: {
        html: 'app/*.html',
        htmlTemplate: 'app/template/*.html',
        js: 'app/js/*.js',
        jsmain: 'app/js/*.js',
        sass: 'app/sass/*.sass',
        css: 'app/css/*.css',
        img: 'app/img/**/*.*',
        fonts: 'app/fonts/**/*.*',
        otherprojects: 'app/otherprojects/**/*.*'
    },
    clean: './dist/'
};

const config = {
    server: {
        baseDir: './dist'
    },
};

gulp.task('otherprojects', function(done) {
    gulp.src(path.app.otherprojects) // выбор всех папок по указанному пути
        .pipe(gulp.dest(path.dist.otherprojects)) // выкладывание готовых файлов
        .pipe(browserSync.reload({ stream: true })); // перезагрузка сервера
    done();
});

gulp.task('browserSync', function(done) {
    browserSync.init(config);
    done();
});


gulp.task('html:build', function(done) {
    gulp.src(path.app.html) // выбор всех html файлов по указанному пути
        .pipe(plumber()) // отслеживание ошибок
        .pipe(rigger()) // импорт вложений
        .pipe(gulp.dest(path.dist.html)) // выкладывание готовых файлов
        .pipe(browserSync.reload({ stream: true })); // перезагрузка сервера
    done();
});


gulp.task('sass:build', function(done) {
    gulp.src(path.app.sass)
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(sourcemaps.init()) // инициализируем sourcemap
        .pipe(sass({outputStyle: 'extended'}).on('error', sass.logError))
        .pipe(postcss([autoprefixer({ overrideBrowserslist: ['last 10 version'] })]))
        .pipe(sourcemaps.write('./')) // записываем sourcemap
        .pipe(gulp.dest(path.app.css))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream({once: true}));
    done();
});


gulp.task('jsmain:build', function(done) {
    gulp.src(path.app.jsmain) // получим файл main.js
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(rigger()) // импортируем все указанные файлы в main.js
        .pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(uglify()) // минимизируем js
        .pipe(sourcemaps.write('./')) //  записываем sourcemap
        .pipe(gulp.dest(path.dist.jsmain)) // положим готовый файл
        .pipe(browserSync.reload({ stream: true })); // перезагрузим сервер
    done();
});


gulp.task('js:build', function(done) {
    gulp.src([path.app.js, '!app/js/main.js'])
        .pipe(gulp.dest(path.dist.js)); // Переносим скрипты в продакшен
    done();
});


gulp.task('fonts:build', function(done) {
    gulp.src(path.app.fonts)
        .pipe(gulp.dest(path.dist.fonts));
    done();
});


gulp.task('image:build', function(done) {
    gulp.src(path.app.img)
        .pipe(cache(imagemin([
            imagemin.gifsicle({ interlaced: true }),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            imagemin.svgo({ plugins: [{ removeViewBox: false }] })
        ])))
        .pipe(gulp.dest(path.dist.img));
    done();
});

// удаление каталога dist
gulp.task('clean:build', function(done) {
    del.sync(path.clean);
    done();
});

// очистка кэша
gulp.task('cache:clear', function(done) {
    cache.clearAll();
    done();
});

// сборка
gulp.task('build',
    gulp.series('clean:build', 'html:build', 'sass:build', 'js:build', 'jsmain:build', 'fonts:build', 'image:build', 'otherprojects', function(done) { //'css:build',
    done();
}));


// запуск задач при изменении файлов
gulp.task('watch', function() {
    gulp.watch(path.watch.html, gulp.series('html:build'));
    gulp.watch(path.watch.htmlTemplate, gulp.series('html:build'));
    gulp.watch(path.watch.sass, gulp.series('sass:build'));
    gulp.watch(path.watch.js, gulp.series('js:build'));
    gulp.watch(path.watch.img, gulp.series('image:build'));
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
    gulp.watch(path.watch.jsmain, gulp.series('jsmain:build'));
    gulp.watch(path.watch.otherprojects, gulp.series('otherprojects'));
});

// задача по умолчанию
gulp.task('default', gulp.series('clean:build', 'build', gulp.parallel('browserSync', 'watch')));
